﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    // Config params
    [SerializeField] float ScreenWidthInUnits = 15f;
    [SerializeField] float paddlePosMin = 1f;
    [SerializeField] float paddlePosMax = 16f;

    // Cached references
    GameSession gameSession;
    Ball ball;

    // Use this for initialization
    void Start () {
        gameSession = FindObjectOfType<GameSession>();
        ball = FindObjectOfType<Ball>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
        paddlePos.x = Mathf.Clamp(GetXPos(), paddlePosMin, paddlePosMax);
        transform.position = paddlePos;
	}

    private float GetXPos()
    {
        if (gameSession.IsAutoPlayEnabled())
        {
            return ball.transform.position.x;
        }
        else
        {
            return Input.mousePosition.x / Screen.width * ScreenWidthInUnits;
        }
    }
}
